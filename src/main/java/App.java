import antlr.*;
import compiler.NixTreeWalkListener;
import compiler.utils.bytecode.Instruction;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.objectweb.asm.Opcodes;


public class App{
    public static void main(String[] args) throws IOException {
        nixlangLexer lexer =  new nixlangLexer( new ANTLRInputStream("print('Hello guys this is nixes language aka nixlang saying hello world to you via the JVM')"));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        nixlangParser parser = new nixlangParser(tokens);
        NixTreeWalkListener listener = new NixTreeWalkListener();
        parser.addParseListener(listener);
        parser.prog();
        for (Instruction instruction : listener.getInstructionQueue()) {
            instruction.apply(listener.mv);
        }
        final String classFile = "Test.class";
        OutputStream os = new FileOutputStream(classFile);
        listener.mv.visitInsn(Opcodes.RETURN);
        listener.mv.visitMaxs(100, 100);
        listener.mv.visitEnd();
        listener.cw.visitEnd();
        os.write(listener.cw.toByteArray());
        os.close();

    }
}
