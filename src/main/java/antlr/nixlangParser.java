// Generated from nixlang.g4 by ANTLR 4.5.3

package antlr;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class nixlangParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, IDENTIFIER=21, ASSIGN=22, ADD_ASSIGN=23, 
		SUB_ASSIGN=24, MUL_ASSIGN=25, DIV_ASSIGN=26, DIGIT=27, NONE=28, NEWLINE=29, 
		PLUS=30, MINUS=31, LBRACE=32, RBRACE=33, LPAREN=34, RPAREN=35, COLON=36, 
		COMMENT=37, WS=38;
	public static final int
		RULE_prog = 0, RULE_block = 1, RULE_blockElement = 2, RULE_statement = 3, 
		RULE_forStmnt = 4, RULE_ifStmnt = 5, RULE_whileStmnt = 6, RULE_ifThenStmnt = 7, 
		RULE_elseStmnt = 8, RULE_elifStmnt = 9, RULE_assignment = 10, RULE_assignOperator = 11, 
		RULE_binaryOp = 12, RULE_methodCall = 13, RULE_builtinMethod = 14, RULE_expression = 15, 
		RULE_arraySliceExpression = 16, RULE_arrayExpression = 17, RULE_unaryExpression = 18, 
		RULE_expressionName = 19, RULE_ambiguousName = 20, RULE_primary = 21, 
		RULE_literal = 22, RULE_bool = 23, RULE_string = 24, RULE_floatNumber = 25, 
		RULE_integer = 26;
	public static final String[] ruleNames = {
		"prog", "block", "blockElement", "statement", "forStmnt", "ifStmnt", "whileStmnt", 
		"ifThenStmnt", "elseStmnt", "elifStmnt", "assignment", "assignOperator", 
		"binaryOp", "methodCall", "builtinMethod", "expression", "arraySliceExpression", 
		"arrayExpression", "unaryExpression", "expressionName", "ambiguousName", 
		"primary", "literal", "bool", "string", "floatNumber", "integer"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'for'", "'in'", "'while'", "'if'", "'else'", "'elif'", "'*'", "'/'", 
		"'%'", "','", "'print'", "'['", "']'", "':'", "'.'", "'true'", "'false'", 
		"'\"'", "' '", "'''", null, "'='", "'+='", "'-='", "'*='", "'/='", null, 
		"'None'", "'\n'", "'+'", "'-'", "'{'", "'}'", "'('", "')'", "';'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "IDENTIFIER", "ASSIGN", 
		"ADD_ASSIGN", "SUB_ASSIGN", "MUL_ASSIGN", "DIV_ASSIGN", "DIGIT", "NONE", 
		"NEWLINE", "PLUS", "MINUS", "LBRACE", "RBRACE", "LPAREN", "RPAREN", "COLON", 
		"COMMENT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "nixlang.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public nixlangParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitProg(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << T__3) | (1L << T__10) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(54);
				statement();
				}
				}
				setState(59);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public List<BlockElementContext> blockElement() {
			return getRuleContexts(BlockElementContext.class);
		}
		public BlockElementContext blockElement(int i) {
			return getRuleContext(BlockElementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitBlock(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(63);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << T__3) | (1L << T__10) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(60);
				blockElement();
				}
				}
				setState(65);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockElementContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public BlockElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockElement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterBlockElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitBlockElement(this);
		}
	}

	public final BlockElementContext blockElement() throws RecognitionException {
		BlockElementContext _localctx = new BlockElementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_blockElement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public IfStmntContext ifStmnt() {
			return getRuleContext(IfStmntContext.class,0);
		}
		public WhileStmntContext whileStmnt() {
			return getRuleContext(WhileStmntContext.class,0);
		}
		public ForStmntContext forStmnt() {
			return getRuleContext(ForStmntContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public MethodCallContext methodCall() {
			return getRuleContext(MethodCallContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_statement);
		try {
			setState(73);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(68);
				ifStmnt();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(69);
				whileStmnt();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(70);
				forStmnt();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(71);
				assignment();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(72);
				methodCall();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForStmntContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(nixlangParser.LPAREN, 0); }
		public TerminalNode IDENTIFIER() { return getToken(nixlangParser.IDENTIFIER, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(nixlangParser.RPAREN, 0); }
		public TerminalNode LBRACE() { return getToken(nixlangParser.LBRACE, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(nixlangParser.RBRACE, 0); }
		public List<TerminalNode> NEWLINE() { return getTokens(nixlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(nixlangParser.NEWLINE, i);
		}
		public ForStmntContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forStmnt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterForStmnt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitForStmnt(this);
		}
	}

	public final ForStmntContext forStmnt() throws RecognitionException {
		ForStmntContext _localctx = new ForStmntContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_forStmnt);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			match(T__0);
			setState(79);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(76);
				match(NEWLINE);
				}
				}
				setState(81);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(82);
			match(LPAREN);
			setState(86);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(83);
				match(NEWLINE);
				}
				}
				setState(88);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(89);
			match(IDENTIFIER);
			setState(90);
			match(T__1);
			setState(91);
			expression(0);
			setState(95);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(92);
				match(NEWLINE);
				}
				}
				setState(97);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(98);
			match(RPAREN);
			setState(102);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(99);
				match(NEWLINE);
				}
				}
				setState(104);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(105);
			match(LBRACE);
			setState(109);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(106);
					match(NEWLINE);
					}
					} 
				}
				setState(111);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			}
			setState(112);
			block();
			setState(116);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(113);
				match(NEWLINE);
				}
				}
				setState(118);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(119);
			match(RBRACE);
			setState(123);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(120);
					match(NEWLINE);
					}
					} 
				}
				setState(125);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStmntContext extends ParserRuleContext {
		public IfThenStmntContext ifThenStmnt() {
			return getRuleContext(IfThenStmntContext.class,0);
		}
		public List<ElifStmntContext> elifStmnt() {
			return getRuleContexts(ElifStmntContext.class);
		}
		public ElifStmntContext elifStmnt(int i) {
			return getRuleContext(ElifStmntContext.class,i);
		}
		public ElseStmntContext elseStmnt() {
			return getRuleContext(ElseStmntContext.class,0);
		}
		public IfStmntContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStmnt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterIfStmnt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitIfStmnt(this);
		}
	}

	public final IfStmntContext ifStmnt() throws RecognitionException {
		IfStmntContext _localctx = new IfStmntContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_ifStmnt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(126);
			ifThenStmnt();
			setState(130);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(127);
				elifStmnt();
				}
				}
				setState(132);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(134);
			_la = _input.LA(1);
			if (_la==T__4) {
				{
				setState(133);
				elseStmnt();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStmntContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(nixlangParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(nixlangParser.RPAREN, 0); }
		public TerminalNode LBRACE() { return getToken(nixlangParser.LBRACE, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(nixlangParser.RBRACE, 0); }
		public List<TerminalNode> NEWLINE() { return getTokens(nixlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(nixlangParser.NEWLINE, i);
		}
		public WhileStmntContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileStmnt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterWhileStmnt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitWhileStmnt(this);
		}
	}

	public final WhileStmntContext whileStmnt() throws RecognitionException {
		WhileStmntContext _localctx = new WhileStmntContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_whileStmnt);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(136);
			match(T__2);
			setState(140);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(137);
				match(NEWLINE);
				}
				}
				setState(142);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(143);
			match(LPAREN);
			setState(147);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(144);
				match(NEWLINE);
				}
				}
				setState(149);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(150);
			expression(0);
			setState(154);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(151);
				match(NEWLINE);
				}
				}
				setState(156);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(157);
			match(RPAREN);
			setState(161);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(158);
				match(NEWLINE);
				}
				}
				setState(163);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(164);
			match(LBRACE);
			setState(168);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(165);
					match(NEWLINE);
					}
					} 
				}
				setState(170);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			setState(171);
			block();
			setState(175);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(172);
				match(NEWLINE);
				}
				}
				setState(177);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(178);
			match(RBRACE);
			setState(182);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(179);
					match(NEWLINE);
					}
					} 
				}
				setState(184);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfThenStmntContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(nixlangParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(nixlangParser.RPAREN, 0); }
		public TerminalNode LBRACE() { return getToken(nixlangParser.LBRACE, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(nixlangParser.RBRACE, 0); }
		public List<TerminalNode> NEWLINE() { return getTokens(nixlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(nixlangParser.NEWLINE, i);
		}
		public IfThenStmntContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifThenStmnt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterIfThenStmnt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitIfThenStmnt(this);
		}
	}

	public final IfThenStmntContext ifThenStmnt() throws RecognitionException {
		IfThenStmntContext _localctx = new IfThenStmntContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_ifThenStmnt);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			match(T__3);
			setState(189);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(186);
				match(NEWLINE);
				}
				}
				setState(191);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(192);
			match(LPAREN);
			setState(196);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(193);
				match(NEWLINE);
				}
				}
				setState(198);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(199);
			expression(0);
			setState(203);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(200);
				match(NEWLINE);
				}
				}
				setState(205);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(206);
			match(RPAREN);
			setState(210);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(207);
				match(NEWLINE);
				}
				}
				setState(212);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(213);
			match(LBRACE);
			setState(217);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(214);
					match(NEWLINE);
					}
					} 
				}
				setState(219);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			setState(220);
			block();
			setState(224);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(221);
				match(NEWLINE);
				}
				}
				setState(226);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(227);
			match(RBRACE);
			setState(231);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(228);
					match(NEWLINE);
					}
					} 
				}
				setState(233);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseStmntContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(nixlangParser.LBRACE, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(nixlangParser.RBRACE, 0); }
		public List<TerminalNode> NEWLINE() { return getTokens(nixlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(nixlangParser.NEWLINE, i);
		}
		public ElseStmntContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseStmnt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterElseStmnt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitElseStmnt(this);
		}
	}

	public final ElseStmntContext elseStmnt() throws RecognitionException {
		ElseStmntContext _localctx = new ElseStmntContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_elseStmnt);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(234);
			match(T__4);
			setState(238);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(235);
				match(NEWLINE);
				}
				}
				setState(240);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(241);
			match(LBRACE);
			setState(245);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(242);
					match(NEWLINE);
					}
					} 
				}
				setState(247);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			}
			setState(248);
			block();
			setState(252);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(249);
				match(NEWLINE);
				}
				}
				setState(254);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(255);
			match(RBRACE);
			setState(259);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(256);
					match(NEWLINE);
					}
					} 
				}
				setState(261);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElifStmntContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(nixlangParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(nixlangParser.RPAREN, 0); }
		public TerminalNode LBRACE() { return getToken(nixlangParser.LBRACE, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(nixlangParser.RBRACE, 0); }
		public List<TerminalNode> NEWLINE() { return getTokens(nixlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(nixlangParser.NEWLINE, i);
		}
		public ElifStmntContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elifStmnt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterElifStmnt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitElifStmnt(this);
		}
	}

	public final ElifStmntContext elifStmnt() throws RecognitionException {
		ElifStmntContext _localctx = new ElifStmntContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_elifStmnt);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(262);
			match(T__5);
			setState(266);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(263);
				match(NEWLINE);
				}
				}
				setState(268);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(269);
			match(LPAREN);
			setState(273);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(270);
				match(NEWLINE);
				}
				}
				setState(275);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(276);
			expression(0);
			setState(280);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(277);
				match(NEWLINE);
				}
				}
				setState(282);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(283);
			match(RPAREN);
			setState(287);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(284);
				match(NEWLINE);
				}
				}
				setState(289);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(290);
			match(LBRACE);
			setState(294);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(291);
					match(NEWLINE);
					}
					} 
				}
				setState(296);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			}
			setState(297);
			block();
			setState(301);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NEWLINE) {
				{
				{
				setState(298);
				match(NEWLINE);
				}
				}
				setState(303);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(304);
			match(RBRACE);
			setState(308);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(305);
					match(NEWLINE);
					}
					} 
				}
				setState(310);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(nixlangParser.IDENTIFIER, 0); }
		public AssignOperatorContext assignOperator() {
			return getRuleContext(AssignOperatorContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(nixlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(nixlangParser.NEWLINE, i);
		}
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitAssignment(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_assignment);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(311);
			match(IDENTIFIER);
			setState(312);
			assignOperator();
			setState(313);
			expression(0);
			setState(317);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1+1 ) {
					{
					{
					setState(314);
					match(NEWLINE);
					}
					} 
				}
				setState(319);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignOperatorContext extends ParserRuleContext {
		public TerminalNode ASSIGN() { return getToken(nixlangParser.ASSIGN, 0); }
		public TerminalNode ADD_ASSIGN() { return getToken(nixlangParser.ADD_ASSIGN, 0); }
		public TerminalNode DIV_ASSIGN() { return getToken(nixlangParser.DIV_ASSIGN, 0); }
		public TerminalNode MUL_ASSIGN() { return getToken(nixlangParser.MUL_ASSIGN, 0); }
		public TerminalNode SUB_ASSIGN() { return getToken(nixlangParser.SUB_ASSIGN, 0); }
		public AssignOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterAssignOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitAssignOperator(this);
		}
	}

	public final AssignOperatorContext assignOperator() throws RecognitionException {
		AssignOperatorContext _localctx = new AssignOperatorContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_assignOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(320);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ASSIGN) | (1L << ADD_ASSIGN) | (1L << SUB_ASSIGN) | (1L << MUL_ASSIGN) | (1L << DIV_ASSIGN))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BinaryOpContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(nixlangParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(nixlangParser.MINUS, 0); }
		public BinaryOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binaryOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterBinaryOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitBinaryOp(this);
		}
	}

	public final BinaryOpContext binaryOp() throws RecognitionException {
		BinaryOpContext _localctx = new BinaryOpContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_binaryOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(322);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << PLUS) | (1L << MINUS))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodCallContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(nixlangParser.IDENTIFIER, 0); }
		public BuiltinMethodContext builtinMethod() {
			return getRuleContext(BuiltinMethodContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(nixlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(nixlangParser.NEWLINE, i);
		}
		public MethodCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterMethodCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitMethodCall(this);
		}
	}

	public final MethodCallContext methodCall() throws RecognitionException {
		MethodCallContext _localctx = new MethodCallContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_methodCall);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(326);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				{
				setState(324);
				match(IDENTIFIER);
				}
				break;
			case T__10:
				{
				setState(325);
				builtinMethod();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(328);
			match(LPAREN);
			setState(330);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__19) | (1L << IDENTIFIER) | (1L << DIGIT) | (1L << NONE) | (1L << PLUS) | (1L << MINUS) | (1L << LPAREN))) != 0)) {
				{
				setState(329);
				expression(0);
				}
			}

			setState(336);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__9) {
				{
				{
				setState(332);
				match(T__9);
				setState(333);
				expression(0);
				}
				}
				setState(338);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(339);
			match(RPAREN);
			setState(343);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(340);
					match(NEWLINE);
					}
					} 
				}
				setState(345);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BuiltinMethodContext extends ParserRuleContext {
		public BuiltinMethodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtinMethod; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterBuiltinMethod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitBuiltinMethod(this);
		}
	}

	public final BuiltinMethodContext builtinMethod() throws RecognitionException {
		BuiltinMethodContext _localctx = new BuiltinMethodContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_builtinMethod);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(346);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ArrayExpressionContext arrayExpression() {
			return getRuleContext(ArrayExpressionContext.class,0);
		}
		public ArraySliceExpressionContext arraySliceExpression() {
			return getRuleContext(ArraySliceExpressionContext.class,0);
		}
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public BinaryOpContext binaryOp() {
			return getRuleContext(BinaryOpContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(352);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
			case 1:
				{
				setState(349);
				arrayExpression();
				}
				break;
			case 2:
				{
				setState(350);
				arraySliceExpression();
				}
				break;
			case 3:
				{
				setState(351);
				unaryExpression();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(360);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_expression);
					setState(354);
					if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
					setState(355);
					binaryOp();
					setState(356);
					expression(5);
					}
					} 
				}
				setState(362);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ArraySliceExpressionContext extends ParserRuleContext {
		public List<IntegerContext> integer() {
			return getRuleContexts(IntegerContext.class);
		}
		public IntegerContext integer(int i) {
			return getRuleContext(IntegerContext.class,i);
		}
		public ExpressionNameContext expressionName() {
			return getRuleContext(ExpressionNameContext.class,0);
		}
		public ArrayExpressionContext arrayExpression() {
			return getRuleContext(ArrayExpressionContext.class,0);
		}
		public ArraySliceExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arraySliceExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterArraySliceExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitArraySliceExpression(this);
		}
	}

	public final ArraySliceExpressionContext arraySliceExpression() throws RecognitionException {
		ArraySliceExpressionContext _localctx = new ArraySliceExpressionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_arraySliceExpression);
		try {
			setState(381);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(365);
				switch (_input.LA(1)) {
				case IDENTIFIER:
					{
					setState(363);
					expressionName();
					}
					break;
				case T__11:
					{
					setState(364);
					arrayExpression();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(367);
				match(T__11);
				setState(368);
				integer();
				setState(369);
				match(T__12);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(373);
				switch (_input.LA(1)) {
				case IDENTIFIER:
					{
					setState(371);
					expressionName();
					}
					break;
				case T__11:
					{
					setState(372);
					arrayExpression();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(375);
				match(T__11);
				setState(376);
				integer();
				setState(377);
				match(T__13);
				setState(378);
				integer();
				setState(379);
				match(T__12);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayExpressionContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ArrayExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterArrayExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitArrayExpression(this);
		}
	}

	public final ArrayExpressionContext arrayExpression() throws RecognitionException {
		ArrayExpressionContext _localctx = new ArrayExpressionContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_arrayExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(383);
			match(T__11);
			setState(384);
			expression(0);
			setState(389);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__9) {
				{
				{
				setState(385);
				match(T__9);
				setState(386);
				expression(0);
				}
				}
				setState(391);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(392);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryExpressionContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(nixlangParser.PLUS, 0); }
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public TerminalNode MINUS() { return getToken(nixlangParser.MINUS, 0); }
		public PrimaryContext primary() {
			return getRuleContext(PrimaryContext.class,0);
		}
		public ExpressionNameContext expressionName() {
			return getRuleContext(ExpressionNameContext.class,0);
		}
		public UnaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterUnaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitUnaryExpression(this);
		}
	}

	public final UnaryExpressionContext unaryExpression() throws RecognitionException {
		UnaryExpressionContext _localctx = new UnaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_unaryExpression);
		try {
			setState(402);
			switch (_input.LA(1)) {
			case PLUS:
				enterOuterAlt(_localctx, 1);
				{
				setState(394);
				match(PLUS);
				setState(395);
				unaryExpression();
				}
				break;
			case MINUS:
				enterOuterAlt(_localctx, 2);
				{
				setState(396);
				match(MINUS);
				setState(397);
				unaryExpression();
				}
				break;
			case T__14:
			case T__15:
			case T__16:
			case T__17:
			case T__19:
			case IDENTIFIER:
			case DIGIT:
			case NONE:
			case LPAREN:
				enterOuterAlt(_localctx, 3);
				{
				setState(400);
				switch (_input.LA(1)) {
				case T__14:
				case T__15:
				case T__16:
				case T__17:
				case T__19:
				case DIGIT:
				case NONE:
				case LPAREN:
					{
					setState(398);
					primary();
					}
					break;
				case IDENTIFIER:
					{
					setState(399);
					expressionName();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(nixlangParser.IDENTIFIER, 0); }
		public AmbiguousNameContext ambiguousName() {
			return getRuleContext(AmbiguousNameContext.class,0);
		}
		public ExpressionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterExpressionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitExpressionName(this);
		}
	}

	public final ExpressionNameContext expressionName() throws RecognitionException {
		ExpressionNameContext _localctx = new ExpressionNameContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_expressionName);
		try {
			setState(409);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(404);
				match(IDENTIFIER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(405);
				ambiguousName(0);
				setState(406);
				match(T__14);
				setState(407);
				match(IDENTIFIER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AmbiguousNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(nixlangParser.IDENTIFIER, 0); }
		public AmbiguousNameContext ambiguousName() {
			return getRuleContext(AmbiguousNameContext.class,0);
		}
		public AmbiguousNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ambiguousName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterAmbiguousName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitAmbiguousName(this);
		}
	}

	public final AmbiguousNameContext ambiguousName() throws RecognitionException {
		return ambiguousName(0);
	}

	private AmbiguousNameContext ambiguousName(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AmbiguousNameContext _localctx = new AmbiguousNameContext(_ctx, _parentState);
		AmbiguousNameContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_ambiguousName, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(412);
			match(IDENTIFIER);
			}
			_ctx.stop = _input.LT(-1);
			setState(419);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AmbiguousNameContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_ambiguousName);
					setState(414);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(415);
					match(T__14);
					setState(416);
					match(IDENTIFIER);
					}
					} 
				}
				setState(421);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PrimaryContext extends ParserRuleContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public PrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterPrimary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitPrimary(this);
		}
	}

	public final PrimaryContext primary() throws RecognitionException {
		PrimaryContext _localctx = new PrimaryContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_primary);
		try {
			setState(427);
			switch (_input.LA(1)) {
			case T__14:
			case T__15:
			case T__16:
			case T__17:
			case T__19:
			case DIGIT:
			case NONE:
				enterOuterAlt(_localctx, 1);
				{
				setState(422);
				literal();
				}
				break;
			case LPAREN:
				enterOuterAlt(_localctx, 2);
				{
				setState(423);
				match(LPAREN);
				setState(424);
				expression(0);
				setState(425);
				match(RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public IntegerContext integer() {
			return getRuleContext(IntegerContext.class,0);
		}
		public FloatNumberContext floatNumber() {
			return getRuleContext(FloatNumberContext.class,0);
		}
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode NONE() { return getToken(nixlangParser.NONE, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitLiteral(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_literal);
		try {
			setState(434);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(429);
				integer();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(430);
				floatNumber();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(431);
				bool();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(432);
				string();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(433);
				match(NONE);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolContext extends ParserRuleContext {
		public BoolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterBool(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitBool(this);
		}
	}

	public final BoolContext bool() throws RecognitionException {
		BoolContext _localctx = new BoolContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_bool);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(436);
			_la = _input.LA(1);
			if ( !(_la==T__15 || _la==T__16) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitString(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_string);
		try {
			int _alt;
			setState(456);
			switch (_input.LA(1)) {
			case T__17:
				enterOuterAlt(_localctx, 1);
				{
				setState(438);
				match(T__17);
				setState(443);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,55,_ctx);
				while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1+1 ) {
						{
						setState(441);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,54,_ctx) ) {
						case 1:
							{
							setState(439);
							matchWildcard();
							}
							break;
						case 2:
							{
							setState(440);
							match(T__18);
							}
							break;
						}
						} 
					}
					setState(445);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,55,_ctx);
				}
				setState(446);
				match(T__17);
				}
				break;
			case T__19:
				enterOuterAlt(_localctx, 2);
				{
				setState(447);
				match(T__19);
				setState(452);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,57,_ctx);
				while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1+1 ) {
						{
						setState(450);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,56,_ctx) ) {
						case 1:
							{
							setState(448);
							matchWildcard();
							}
							break;
						case 2:
							{
							setState(449);
							match(T__18);
							}
							break;
						}
						} 
					}
					setState(454);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,57,_ctx);
				}
				setState(455);
				match(T__19);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FloatNumberContext extends ParserRuleContext {
		public List<TerminalNode> DIGIT() { return getTokens(nixlangParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(nixlangParser.DIGIT, i);
		}
		public FloatNumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_floatNumber; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterFloatNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitFloatNumber(this);
		}
	}

	public final FloatNumberContext floatNumber() throws RecognitionException {
		FloatNumberContext _localctx = new FloatNumberContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_floatNumber);
		int _la;
		try {
			int _alt;
			setState(475);
			switch (_input.LA(1)) {
			case DIGIT:
				enterOuterAlt(_localctx, 1);
				{
				setState(459); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(458);
					match(DIGIT);
					}
					}
					setState(461); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==DIGIT );
				setState(463);
				match(T__14);
				setState(465); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(464);
						match(DIGIT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(467); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,60,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case T__14:
				enterOuterAlt(_localctx, 2);
				{
				setState(469);
				match(T__14);
				setState(471); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(470);
						match(DIGIT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(473); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,61,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerContext extends ParserRuleContext {
		public List<TerminalNode> DIGIT() { return getTokens(nixlangParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(nixlangParser.DIGIT, i);
		}
		public IntegerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).enterInteger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof nixlangListener ) ((nixlangListener)listener).exitInteger(this);
		}
	}

	public final IntegerContext integer() throws RecognitionException {
		IntegerContext _localctx = new IntegerContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_integer);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(478); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(477);
					match(DIGIT);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(480); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,63,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 15:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		case 20:
			return ambiguousName_sempred((AmbiguousNameContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		}
		return true;
	}
	private boolean ambiguousName_sempred(AmbiguousNameContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3(\u01e5\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\3\2\7\2:\n\2\f\2\16\2=\13\2\3\3\7\3@\n"+
		"\3\f\3\16\3C\13\3\3\4\3\4\3\5\3\5\3\5\3\5\3\5\5\5L\n\5\3\6\3\6\7\6P\n"+
		"\6\f\6\16\6S\13\6\3\6\3\6\7\6W\n\6\f\6\16\6Z\13\6\3\6\3\6\3\6\3\6\7\6"+
		"`\n\6\f\6\16\6c\13\6\3\6\3\6\7\6g\n\6\f\6\16\6j\13\6\3\6\3\6\7\6n\n\6"+
		"\f\6\16\6q\13\6\3\6\3\6\7\6u\n\6\f\6\16\6x\13\6\3\6\3\6\7\6|\n\6\f\6\16"+
		"\6\177\13\6\3\7\3\7\7\7\u0083\n\7\f\7\16\7\u0086\13\7\3\7\5\7\u0089\n"+
		"\7\3\b\3\b\7\b\u008d\n\b\f\b\16\b\u0090\13\b\3\b\3\b\7\b\u0094\n\b\f\b"+
		"\16\b\u0097\13\b\3\b\3\b\7\b\u009b\n\b\f\b\16\b\u009e\13\b\3\b\3\b\7\b"+
		"\u00a2\n\b\f\b\16\b\u00a5\13\b\3\b\3\b\7\b\u00a9\n\b\f\b\16\b\u00ac\13"+
		"\b\3\b\3\b\7\b\u00b0\n\b\f\b\16\b\u00b3\13\b\3\b\3\b\7\b\u00b7\n\b\f\b"+
		"\16\b\u00ba\13\b\3\t\3\t\7\t\u00be\n\t\f\t\16\t\u00c1\13\t\3\t\3\t\7\t"+
		"\u00c5\n\t\f\t\16\t\u00c8\13\t\3\t\3\t\7\t\u00cc\n\t\f\t\16\t\u00cf\13"+
		"\t\3\t\3\t\7\t\u00d3\n\t\f\t\16\t\u00d6\13\t\3\t\3\t\7\t\u00da\n\t\f\t"+
		"\16\t\u00dd\13\t\3\t\3\t\7\t\u00e1\n\t\f\t\16\t\u00e4\13\t\3\t\3\t\7\t"+
		"\u00e8\n\t\f\t\16\t\u00eb\13\t\3\n\3\n\7\n\u00ef\n\n\f\n\16\n\u00f2\13"+
		"\n\3\n\3\n\7\n\u00f6\n\n\f\n\16\n\u00f9\13\n\3\n\3\n\7\n\u00fd\n\n\f\n"+
		"\16\n\u0100\13\n\3\n\3\n\7\n\u0104\n\n\f\n\16\n\u0107\13\n\3\13\3\13\7"+
		"\13\u010b\n\13\f\13\16\13\u010e\13\13\3\13\3\13\7\13\u0112\n\13\f\13\16"+
		"\13\u0115\13\13\3\13\3\13\7\13\u0119\n\13\f\13\16\13\u011c\13\13\3\13"+
		"\3\13\7\13\u0120\n\13\f\13\16\13\u0123\13\13\3\13\3\13\7\13\u0127\n\13"+
		"\f\13\16\13\u012a\13\13\3\13\3\13\7\13\u012e\n\13\f\13\16\13\u0131\13"+
		"\13\3\13\3\13\7\13\u0135\n\13\f\13\16\13\u0138\13\13\3\f\3\f\3\f\3\f\7"+
		"\f\u013e\n\f\f\f\16\f\u0141\13\f\3\r\3\r\3\16\3\16\3\17\3\17\5\17\u0149"+
		"\n\17\3\17\3\17\5\17\u014d\n\17\3\17\3\17\7\17\u0151\n\17\f\17\16\17\u0154"+
		"\13\17\3\17\3\17\7\17\u0158\n\17\f\17\16\17\u015b\13\17\3\20\3\20\3\21"+
		"\3\21\3\21\3\21\5\21\u0163\n\21\3\21\3\21\3\21\3\21\7\21\u0169\n\21\f"+
		"\21\16\21\u016c\13\21\3\22\3\22\5\22\u0170\n\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\5\22\u0178\n\22\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u0180\n\22"+
		"\3\23\3\23\3\23\3\23\7\23\u0186\n\23\f\23\16\23\u0189\13\23\3\23\3\23"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u0193\n\24\5\24\u0195\n\24\3\25\3"+
		"\25\3\25\3\25\3\25\5\25\u019c\n\25\3\26\3\26\3\26\3\26\3\26\3\26\7\26"+
		"\u01a4\n\26\f\26\16\26\u01a7\13\26\3\27\3\27\3\27\3\27\3\27\5\27\u01ae"+
		"\n\27\3\30\3\30\3\30\3\30\3\30\5\30\u01b5\n\30\3\31\3\31\3\32\3\32\3\32"+
		"\7\32\u01bc\n\32\f\32\16\32\u01bf\13\32\3\32\3\32\3\32\3\32\7\32\u01c5"+
		"\n\32\f\32\16\32\u01c8\13\32\3\32\5\32\u01cb\n\32\3\33\6\33\u01ce\n\33"+
		"\r\33\16\33\u01cf\3\33\3\33\6\33\u01d4\n\33\r\33\16\33\u01d5\3\33\3\33"+
		"\6\33\u01da\n\33\r\33\16\33\u01db\5\33\u01de\n\33\3\34\6\34\u01e1\n\34"+
		"\r\34\16\34\u01e2\3\34\5\u013f\u01bd\u01c6\4 *\35\2\4\6\b\n\f\16\20\22"+
		"\24\26\30\32\34\36 \"$&(*,.\60\62\64\66\2\5\3\2\30\34\4\2\t\13 !\3\2\22"+
		"\23\u0211\2;\3\2\2\2\4A\3\2\2\2\6D\3\2\2\2\bK\3\2\2\2\nM\3\2\2\2\f\u0080"+
		"\3\2\2\2\16\u008a\3\2\2\2\20\u00bb\3\2\2\2\22\u00ec\3\2\2\2\24\u0108\3"+
		"\2\2\2\26\u0139\3\2\2\2\30\u0142\3\2\2\2\32\u0144\3\2\2\2\34\u0148\3\2"+
		"\2\2\36\u015c\3\2\2\2 \u0162\3\2\2\2\"\u017f\3\2\2\2$\u0181\3\2\2\2&\u0194"+
		"\3\2\2\2(\u019b\3\2\2\2*\u019d\3\2\2\2,\u01ad\3\2\2\2.\u01b4\3\2\2\2\60"+
		"\u01b6\3\2\2\2\62\u01ca\3\2\2\2\64\u01dd\3\2\2\2\66\u01e0\3\2\2\28:\5"+
		"\b\5\298\3\2\2\2:=\3\2\2\2;9\3\2\2\2;<\3\2\2\2<\3\3\2\2\2=;\3\2\2\2>@"+
		"\5\6\4\2?>\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2B\5\3\2\2\2CA\3\2\2\2"+
		"DE\5\b\5\2E\7\3\2\2\2FL\5\f\7\2GL\5\16\b\2HL\5\n\6\2IL\5\26\f\2JL\5\34"+
		"\17\2KF\3\2\2\2KG\3\2\2\2KH\3\2\2\2KI\3\2\2\2KJ\3\2\2\2L\t\3\2\2\2MQ\7"+
		"\3\2\2NP\7\37\2\2ON\3\2\2\2PS\3\2\2\2QO\3\2\2\2QR\3\2\2\2RT\3\2\2\2SQ"+
		"\3\2\2\2TX\7$\2\2UW\7\37\2\2VU\3\2\2\2WZ\3\2\2\2XV\3\2\2\2XY\3\2\2\2Y"+
		"[\3\2\2\2ZX\3\2\2\2[\\\7\27\2\2\\]\7\4\2\2]a\5 \21\2^`\7\37\2\2_^\3\2"+
		"\2\2`c\3\2\2\2a_\3\2\2\2ab\3\2\2\2bd\3\2\2\2ca\3\2\2\2dh\7%\2\2eg\7\37"+
		"\2\2fe\3\2\2\2gj\3\2\2\2hf\3\2\2\2hi\3\2\2\2ik\3\2\2\2jh\3\2\2\2ko\7\""+
		"\2\2ln\7\37\2\2ml\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2pr\3\2\2\2qo\3"+
		"\2\2\2rv\5\4\3\2su\7\37\2\2ts\3\2\2\2ux\3\2\2\2vt\3\2\2\2vw\3\2\2\2wy"+
		"\3\2\2\2xv\3\2\2\2y}\7#\2\2z|\7\37\2\2{z\3\2\2\2|\177\3\2\2\2}{\3\2\2"+
		"\2}~\3\2\2\2~\13\3\2\2\2\177}\3\2\2\2\u0080\u0084\5\20\t\2\u0081\u0083"+
		"\5\24\13\2\u0082\u0081\3\2\2\2\u0083\u0086\3\2\2\2\u0084\u0082\3\2\2\2"+
		"\u0084\u0085\3\2\2\2\u0085\u0088\3\2\2\2\u0086\u0084\3\2\2\2\u0087\u0089"+
		"\5\22\n\2\u0088\u0087\3\2\2\2\u0088\u0089\3\2\2\2\u0089\r\3\2\2\2\u008a"+
		"\u008e\7\5\2\2\u008b\u008d\7\37\2\2\u008c\u008b\3\2\2\2\u008d\u0090\3"+
		"\2\2\2\u008e\u008c\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u0091\3\2\2\2\u0090"+
		"\u008e\3\2\2\2\u0091\u0095\7$\2\2\u0092\u0094\7\37\2\2\u0093\u0092\3\2"+
		"\2\2\u0094\u0097\3\2\2\2\u0095\u0093\3\2\2\2\u0095\u0096\3\2\2\2\u0096"+
		"\u0098\3\2\2\2\u0097\u0095\3\2\2\2\u0098\u009c\5 \21\2\u0099\u009b\7\37"+
		"\2\2\u009a\u0099\3\2\2\2\u009b\u009e\3\2\2\2\u009c\u009a\3\2\2\2\u009c"+
		"\u009d\3\2\2\2\u009d\u009f\3\2\2\2\u009e\u009c\3\2\2\2\u009f\u00a3\7%"+
		"\2\2\u00a0\u00a2\7\37\2\2\u00a1\u00a0\3\2\2\2\u00a2\u00a5\3\2\2\2\u00a3"+
		"\u00a1\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a6\3\2\2\2\u00a5\u00a3\3\2"+
		"\2\2\u00a6\u00aa\7\"\2\2\u00a7\u00a9\7\37\2\2\u00a8\u00a7\3\2\2\2\u00a9"+
		"\u00ac\3\2\2\2\u00aa\u00a8\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ad\3\2"+
		"\2\2\u00ac\u00aa\3\2\2\2\u00ad\u00b1\5\4\3\2\u00ae\u00b0\7\37\2\2\u00af"+
		"\u00ae\3\2\2\2\u00b0\u00b3\3\2\2\2\u00b1\u00af\3\2\2\2\u00b1\u00b2\3\2"+
		"\2\2\u00b2\u00b4\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b4\u00b8\7#\2\2\u00b5"+
		"\u00b7\7\37\2\2\u00b6\u00b5\3\2\2\2\u00b7\u00ba\3\2\2\2\u00b8\u00b6\3"+
		"\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\17\3\2\2\2\u00ba\u00b8\3\2\2\2\u00bb"+
		"\u00bf\7\6\2\2\u00bc\u00be\7\37\2\2\u00bd\u00bc\3\2\2\2\u00be\u00c1\3"+
		"\2\2\2\u00bf\u00bd\3\2\2\2\u00bf\u00c0\3\2\2\2\u00c0\u00c2\3\2\2\2\u00c1"+
		"\u00bf\3\2\2\2\u00c2\u00c6\7$\2\2\u00c3\u00c5\7\37\2\2\u00c4\u00c3\3\2"+
		"\2\2\u00c5\u00c8\3\2\2\2\u00c6\u00c4\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7"+
		"\u00c9\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c9\u00cd\5 \21\2\u00ca\u00cc\7\37"+
		"\2\2\u00cb\u00ca\3\2\2\2\u00cc\u00cf\3\2\2\2\u00cd\u00cb\3\2\2\2\u00cd"+
		"\u00ce\3\2\2\2\u00ce\u00d0\3\2\2\2\u00cf\u00cd\3\2\2\2\u00d0\u00d4\7%"+
		"\2\2\u00d1\u00d3\7\37\2\2\u00d2\u00d1\3\2\2\2\u00d3\u00d6\3\2\2\2\u00d4"+
		"\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d7\3\2\2\2\u00d6\u00d4\3\2"+
		"\2\2\u00d7\u00db\7\"\2\2\u00d8\u00da\7\37\2\2\u00d9\u00d8\3\2\2\2\u00da"+
		"\u00dd\3\2\2\2\u00db\u00d9\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00de\3\2"+
		"\2\2\u00dd\u00db\3\2\2\2\u00de\u00e2\5\4\3\2\u00df\u00e1\7\37\2\2\u00e0"+
		"\u00df\3\2\2\2\u00e1\u00e4\3\2\2\2\u00e2\u00e0\3\2\2\2\u00e2\u00e3\3\2"+
		"\2\2\u00e3\u00e5\3\2\2\2\u00e4\u00e2\3\2\2\2\u00e5\u00e9\7#\2\2\u00e6"+
		"\u00e8\7\37\2\2\u00e7\u00e6\3\2\2\2\u00e8\u00eb\3\2\2\2\u00e9\u00e7\3"+
		"\2\2\2\u00e9\u00ea\3\2\2\2\u00ea\21\3\2\2\2\u00eb\u00e9\3\2\2\2\u00ec"+
		"\u00f0\7\7\2\2\u00ed\u00ef\7\37\2\2\u00ee\u00ed\3\2\2\2\u00ef\u00f2\3"+
		"\2\2\2\u00f0\u00ee\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1\u00f3\3\2\2\2\u00f2"+
		"\u00f0\3\2\2\2\u00f3\u00f7\7\"\2\2\u00f4\u00f6\7\37\2\2\u00f5\u00f4\3"+
		"\2\2\2\u00f6\u00f9\3\2\2\2\u00f7\u00f5\3\2\2\2\u00f7\u00f8\3\2\2\2\u00f8"+
		"\u00fa\3\2\2\2\u00f9\u00f7\3\2\2\2\u00fa\u00fe\5\4\3\2\u00fb\u00fd\7\37"+
		"\2\2\u00fc\u00fb\3\2\2\2\u00fd\u0100\3\2\2\2\u00fe\u00fc\3\2\2\2\u00fe"+
		"\u00ff\3\2\2\2\u00ff\u0101\3\2\2\2\u0100\u00fe\3\2\2\2\u0101\u0105\7#"+
		"\2\2\u0102\u0104\7\37\2\2\u0103\u0102\3\2\2\2\u0104\u0107\3\2\2\2\u0105"+
		"\u0103\3\2\2\2\u0105\u0106\3\2\2\2\u0106\23\3\2\2\2\u0107\u0105\3\2\2"+
		"\2\u0108\u010c\7\b\2\2\u0109\u010b\7\37\2\2\u010a\u0109\3\2\2\2\u010b"+
		"\u010e\3\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010d\u010f\3\2"+
		"\2\2\u010e\u010c\3\2\2\2\u010f\u0113\7$\2\2\u0110\u0112\7\37\2\2\u0111"+
		"\u0110\3\2\2\2\u0112\u0115\3\2\2\2\u0113\u0111\3\2\2\2\u0113\u0114\3\2"+
		"\2\2\u0114\u0116\3\2\2\2\u0115\u0113\3\2\2\2\u0116\u011a\5 \21\2\u0117"+
		"\u0119\7\37\2\2\u0118\u0117\3\2\2\2\u0119\u011c\3\2\2\2\u011a\u0118\3"+
		"\2\2\2\u011a\u011b\3\2\2\2\u011b\u011d\3\2\2\2\u011c\u011a\3\2\2\2\u011d"+
		"\u0121\7%\2\2\u011e\u0120\7\37\2\2\u011f\u011e\3\2\2\2\u0120\u0123\3\2"+
		"\2\2\u0121\u011f\3\2\2\2\u0121\u0122\3\2\2\2\u0122\u0124\3\2\2\2\u0123"+
		"\u0121\3\2\2\2\u0124\u0128\7\"\2\2\u0125\u0127\7\37\2\2\u0126\u0125\3"+
		"\2\2\2\u0127\u012a\3\2\2\2\u0128\u0126\3\2\2\2\u0128\u0129\3\2\2\2\u0129"+
		"\u012b\3\2\2\2\u012a\u0128\3\2\2\2\u012b\u012f\5\4\3\2\u012c\u012e\7\37"+
		"\2\2\u012d\u012c\3\2\2\2\u012e\u0131\3\2\2\2\u012f\u012d\3\2\2\2\u012f"+
		"\u0130\3\2\2\2\u0130\u0132\3\2\2\2\u0131\u012f\3\2\2\2\u0132\u0136\7#"+
		"\2\2\u0133\u0135\7\37\2\2\u0134\u0133\3\2\2\2\u0135\u0138\3\2\2\2\u0136"+
		"\u0134\3\2\2\2\u0136\u0137\3\2\2\2\u0137\25\3\2\2\2\u0138\u0136\3\2\2"+
		"\2\u0139\u013a\7\27\2\2\u013a\u013b\5\30\r\2\u013b\u013f\5 \21\2\u013c"+
		"\u013e\7\37\2\2\u013d\u013c\3\2\2\2\u013e\u0141\3\2\2\2\u013f\u0140\3"+
		"\2\2\2\u013f\u013d\3\2\2\2\u0140\27\3\2\2\2\u0141\u013f\3\2\2\2\u0142"+
		"\u0143\t\2\2\2\u0143\31\3\2\2\2\u0144\u0145\t\3\2\2\u0145\33\3\2\2\2\u0146"+
		"\u0149\7\27\2\2\u0147\u0149\5\36\20\2\u0148\u0146\3\2\2\2\u0148\u0147"+
		"\3\2\2\2\u0149\u014a\3\2\2\2\u014a\u014c\7$\2\2\u014b\u014d\5 \21\2\u014c"+
		"\u014b\3\2\2\2\u014c\u014d\3\2\2\2\u014d\u0152\3\2\2\2\u014e\u014f\7\f"+
		"\2\2\u014f\u0151\5 \21\2\u0150\u014e\3\2\2\2\u0151\u0154\3\2\2\2\u0152"+
		"\u0150\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0155\3\2\2\2\u0154\u0152\3\2"+
		"\2\2\u0155\u0159\7%\2\2\u0156\u0158\7\37\2\2\u0157\u0156\3\2\2\2\u0158"+
		"\u015b\3\2\2\2\u0159\u0157\3\2\2\2\u0159\u015a\3\2\2\2\u015a\35\3\2\2"+
		"\2\u015b\u0159\3\2\2\2\u015c\u015d\7\r\2\2\u015d\37\3\2\2\2\u015e\u015f"+
		"\b\21\1\2\u015f\u0163\5$\23\2\u0160\u0163\5\"\22\2\u0161\u0163\5&\24\2"+
		"\u0162\u015e\3\2\2\2\u0162\u0160\3\2\2\2\u0162\u0161\3\2\2\2\u0163\u016a"+
		"\3\2\2\2\u0164\u0165\f\6\2\2\u0165\u0166\5\32\16\2\u0166\u0167\5 \21\7"+
		"\u0167\u0169\3\2\2\2\u0168\u0164\3\2\2\2\u0169\u016c\3\2\2\2\u016a\u0168"+
		"\3\2\2\2\u016a\u016b\3\2\2\2\u016b!\3\2\2\2\u016c\u016a\3\2\2\2\u016d"+
		"\u0170\5(\25\2\u016e\u0170\5$\23\2\u016f\u016d\3\2\2\2\u016f\u016e\3\2"+
		"\2\2\u0170\u0171\3\2\2\2\u0171\u0172\7\16\2\2\u0172\u0173\5\66\34\2\u0173"+
		"\u0174\7\17\2\2\u0174\u0180\3\2\2\2\u0175\u0178\5(\25\2\u0176\u0178\5"+
		"$\23\2\u0177\u0175\3\2\2\2\u0177\u0176\3\2\2\2\u0178\u0179\3\2\2\2\u0179"+
		"\u017a\7\16\2\2\u017a\u017b\5\66\34\2\u017b\u017c\7\20\2\2\u017c\u017d"+
		"\5\66\34\2\u017d\u017e\7\17\2\2\u017e\u0180\3\2\2\2\u017f\u016f\3\2\2"+
		"\2\u017f\u0177\3\2\2\2\u0180#\3\2\2\2\u0181\u0182\7\16\2\2\u0182\u0187"+
		"\5 \21\2\u0183\u0184\7\f\2\2\u0184\u0186\5 \21\2\u0185\u0183\3\2\2\2\u0186"+
		"\u0189\3\2\2\2\u0187\u0185\3\2\2\2\u0187\u0188\3\2\2\2\u0188\u018a\3\2"+
		"\2\2\u0189\u0187\3\2\2\2\u018a\u018b\7\17\2\2\u018b%\3\2\2\2\u018c\u018d"+
		"\7 \2\2\u018d\u0195\5&\24\2\u018e\u018f\7!\2\2\u018f\u0195\5&\24\2\u0190"+
		"\u0193\5,\27\2\u0191\u0193\5(\25\2\u0192\u0190\3\2\2\2\u0192\u0191\3\2"+
		"\2\2\u0193\u0195\3\2\2\2\u0194\u018c\3\2\2\2\u0194\u018e\3\2\2\2\u0194"+
		"\u0192\3\2\2\2\u0195\'\3\2\2\2\u0196\u019c\7\27\2\2\u0197\u0198\5*\26"+
		"\2\u0198\u0199\7\21\2\2\u0199\u019a\7\27\2\2\u019a\u019c\3\2\2\2\u019b"+
		"\u0196\3\2\2\2\u019b\u0197\3\2\2\2\u019c)\3\2\2\2\u019d\u019e\b\26\1\2"+
		"\u019e\u019f\7\27\2\2\u019f\u01a5\3\2\2\2\u01a0\u01a1\f\3\2\2\u01a1\u01a2"+
		"\7\21\2\2\u01a2\u01a4\7\27\2\2\u01a3\u01a0\3\2\2\2\u01a4\u01a7\3\2\2\2"+
		"\u01a5\u01a3\3\2\2\2\u01a5\u01a6\3\2\2\2\u01a6+\3\2\2\2\u01a7\u01a5\3"+
		"\2\2\2\u01a8\u01ae\5.\30\2\u01a9\u01aa\7$\2\2\u01aa\u01ab\5 \21\2\u01ab"+
		"\u01ac\7%\2\2\u01ac\u01ae\3\2\2\2\u01ad\u01a8\3\2\2\2\u01ad\u01a9\3\2"+
		"\2\2\u01ae-\3\2\2\2\u01af\u01b5\5\66\34\2\u01b0\u01b5\5\64\33\2\u01b1"+
		"\u01b5\5\60\31\2\u01b2\u01b5\5\62\32\2\u01b3\u01b5\7\36\2\2\u01b4\u01af"+
		"\3\2\2\2\u01b4\u01b0\3\2\2\2\u01b4\u01b1\3\2\2\2\u01b4\u01b2\3\2\2\2\u01b4"+
		"\u01b3\3\2\2\2\u01b5/\3\2\2\2\u01b6\u01b7\t\4\2\2\u01b7\61\3\2\2\2\u01b8"+
		"\u01bd\7\24\2\2\u01b9\u01bc\13\2\2\2\u01ba\u01bc\7\25\2\2\u01bb\u01b9"+
		"\3\2\2\2\u01bb\u01ba\3\2\2\2\u01bc\u01bf\3\2\2\2\u01bd\u01be\3\2\2\2\u01bd"+
		"\u01bb\3\2\2\2\u01be\u01c0\3\2\2\2\u01bf\u01bd\3\2\2\2\u01c0\u01cb\7\24"+
		"\2\2\u01c1\u01c6\7\26\2\2\u01c2\u01c5\13\2\2\2\u01c3\u01c5\7\25\2\2\u01c4"+
		"\u01c2\3\2\2\2\u01c4\u01c3\3\2\2\2\u01c5\u01c8\3\2\2\2\u01c6\u01c7\3\2"+
		"\2\2\u01c6\u01c4\3\2\2\2\u01c7\u01c9\3\2\2\2\u01c8\u01c6\3\2\2\2\u01c9"+
		"\u01cb\7\26\2\2\u01ca\u01b8\3\2\2\2\u01ca\u01c1\3\2\2\2\u01cb\63\3\2\2"+
		"\2\u01cc\u01ce\7\35\2\2\u01cd\u01cc\3\2\2\2\u01ce\u01cf\3\2\2\2\u01cf"+
		"\u01cd\3\2\2\2\u01cf\u01d0\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d3\7\21"+
		"\2\2\u01d2\u01d4\7\35\2\2\u01d3\u01d2\3\2\2\2\u01d4\u01d5\3\2\2\2\u01d5"+
		"\u01d3\3\2\2\2\u01d5\u01d6\3\2\2\2\u01d6\u01de\3\2\2\2\u01d7\u01d9\7\21"+
		"\2\2\u01d8\u01da\7\35\2\2\u01d9\u01d8\3\2\2\2\u01da\u01db\3\2\2\2\u01db"+
		"\u01d9\3\2\2\2\u01db\u01dc\3\2\2\2\u01dc\u01de\3\2\2\2\u01dd\u01cd\3\2"+
		"\2\2\u01dd\u01d7\3\2\2\2\u01de\65\3\2\2\2\u01df\u01e1\7\35\2\2\u01e0\u01df"+
		"\3\2\2\2\u01e1\u01e2\3\2\2\2\u01e2\u01e0\3\2\2\2\u01e2\u01e3\3\2\2\2\u01e3"+
		"\67\3\2\2\2B;AKQXahov}\u0084\u0088\u008e\u0095\u009c\u00a3\u00aa\u00b1"+
		"\u00b8\u00bf\u00c6\u00cd\u00d4\u00db\u00e2\u00e9\u00f0\u00f7\u00fe\u0105"+
		"\u010c\u0113\u011a\u0121\u0128\u012f\u0136\u013f\u0148\u014c\u0152\u0159"+
		"\u0162\u016a\u016f\u0177\u017f\u0187\u0192\u0194\u019b\u01a5\u01ad\u01b4"+
		"\u01bb\u01bd\u01c4\u01c6\u01ca\u01cf\u01d5\u01db\u01dd\u01e2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}