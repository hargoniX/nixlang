grammar nixlang;

@header {
package antlr;
}

// main prog, empty program is also allowed
prog
    :   statement*
    ;

block
    :   blockElement*
    ;

blockElement
    :   statement
    ;

statement
    :   ifStmnt
    |   whileStmnt
    |   forStmnt
    |   assignment
    |   methodCall
    ;

forStmnt
    :   'for' NEWLINE* LPAREN NEWLINE* IDENTIFIER 'in' expression NEWLINE* RPAREN NEWLINE* LBRACE NEWLINE* block NEWLINE* RBRACE NEWLINE*
    ;

ifStmnt
    :   ifThenStmnt elifStmnt* elseStmnt?
    ;

whileStmnt
    :   'while' NEWLINE* LPAREN NEWLINE* expression NEWLINE* RPAREN NEWLINE* LBRACE NEWLINE* block NEWLINE* RBRACE NEWLINE*
    ;

ifThenStmnt
    :   'if' NEWLINE* LPAREN NEWLINE* expression NEWLINE* RPAREN NEWLINE* LBRACE NEWLINE* block NEWLINE* RBRACE NEWLINE*
    ;

elseStmnt
    :   'else' NEWLINE* LBRACE NEWLINE* block NEWLINE* RBRACE NEWLINE*
    ;

elifStmnt
    :   'elif' NEWLINE* LPAREN NEWLINE* expression NEWLINE* RPAREN NEWLINE* LBRACE NEWLINE* block NEWLINE* RBRACE NEWLINE*
    ;

assignment
    :   IDENTIFIER assignOperator expression NEWLINE*?
    ;

assignOperator
    :   ASSIGN
    |   ADD_ASSIGN
    |   DIV_ASSIGN
    |   MUL_ASSIGN
    |   SUB_ASSIGN
    ;
binaryOp
   /* :   'or'
    |  'and'
    |   '=='
    |   '!='
    |   '<'
    |   '>'
    |   '>='
    |   '<='*/
    :   PLUS
    |   MINUS
    |   '*'
    |   '/'
    |   '%'
    ;

methodCall
    :   (IDENTIFIER|builtinMethod) '(' expression? (',' expression)* ')' NEWLINE*
    ;

builtinMethod
    :   'print'
    ;

expression
    :   expression binaryOp expression
    |   arrayExpression
    |   arraySliceExpression
    |   unaryExpression
    ;

arraySliceExpression
    :   (expressionName|arrayExpression) '[' integer ']'
    |   (expressionName|arrayExpression) '[' integer ':' integer ']'
    ;

arrayExpression
    :   '[' expression (',' expression)* ']'
    ;

unaryExpression
	:	PLUS unaryExpression
	|	MINUS unaryExpression
	|	(primary | expressionName)
	;

expressionName
	:	IDENTIFIER
	|	ambiguousName '.' IDENTIFIER
	;

ambiguousName
	:	IDENTIFIER
	|	ambiguousName '.' IDENTIFIER
	;

primary
    :   literal
    |   '(' expression ')'
    ;

literal
	:	integer
	|	floatNumber
	|	bool
	|	string
	|	NONE
	;

bool
    : 'true'
    | 'false'
    ;

string
    :   '"' (.|' ')*? '"'
    |   '\'' (.|' ')*? '\''
    ;

floatNumber
    :   DIGIT+ '.' DIGIT+
    |   '.' DIGIT+
    ;

integer
    :   DIGIT+
    ;

IDENTIFIER
    :   [a-zA-Z$_][a-zA-Z0-9$_]*
    ;

ASSIGN : '=';

ADD_ASSIGN : '+=';

SUB_ASSIGN : '-=';

MUL_ASSIGN : '*=';

DIV_ASSIGN : '/=';

DIGIT
    :   '0'
    |   [1-9]
    ;

NONE : 'None';

NEWLINE
    :   '\n'
    ;

PLUS    :   '+';

MINUS   :   '-';

LBRACE  :   '{';

RBRACE  :   '}';

LPAREN  :   '(';

RPAREN  :   ')';

COLON   :   ';' -> skip;

COMMENT
    :   '#' ~[\r\n]* -> skip
    ;

WS  :  [ \t\r\u000C]+ -> skip
    ;