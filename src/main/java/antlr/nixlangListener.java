// Generated from nixlang.g4 by ANTLR 4.5.3

package antlr;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link nixlangParser}.
 */
public interface nixlangListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link nixlangParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(nixlangParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(nixlangParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(nixlangParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(nixlangParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#blockElement}.
	 * @param ctx the parse tree
	 */
	void enterBlockElement(nixlangParser.BlockElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#blockElement}.
	 * @param ctx the parse tree
	 */
	void exitBlockElement(nixlangParser.BlockElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(nixlangParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(nixlangParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#forStmnt}.
	 * @param ctx the parse tree
	 */
	void enterForStmnt(nixlangParser.ForStmntContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#forStmnt}.
	 * @param ctx the parse tree
	 */
	void exitForStmnt(nixlangParser.ForStmntContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#ifStmnt}.
	 * @param ctx the parse tree
	 */
	void enterIfStmnt(nixlangParser.IfStmntContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#ifStmnt}.
	 * @param ctx the parse tree
	 */
	void exitIfStmnt(nixlangParser.IfStmntContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#whileStmnt}.
	 * @param ctx the parse tree
	 */
	void enterWhileStmnt(nixlangParser.WhileStmntContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#whileStmnt}.
	 * @param ctx the parse tree
	 */
	void exitWhileStmnt(nixlangParser.WhileStmntContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#ifThenStmnt}.
	 * @param ctx the parse tree
	 */
	void enterIfThenStmnt(nixlangParser.IfThenStmntContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#ifThenStmnt}.
	 * @param ctx the parse tree
	 */
	void exitIfThenStmnt(nixlangParser.IfThenStmntContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#elseStmnt}.
	 * @param ctx the parse tree
	 */
	void enterElseStmnt(nixlangParser.ElseStmntContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#elseStmnt}.
	 * @param ctx the parse tree
	 */
	void exitElseStmnt(nixlangParser.ElseStmntContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#elifStmnt}.
	 * @param ctx the parse tree
	 */
	void enterElifStmnt(nixlangParser.ElifStmntContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#elifStmnt}.
	 * @param ctx the parse tree
	 */
	void exitElifStmnt(nixlangParser.ElifStmntContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(nixlangParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(nixlangParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#assignOperator}.
	 * @param ctx the parse tree
	 */
	void enterAssignOperator(nixlangParser.AssignOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#assignOperator}.
	 * @param ctx the parse tree
	 */
	void exitAssignOperator(nixlangParser.AssignOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#binaryOp}.
	 * @param ctx the parse tree
	 */
	void enterBinaryOp(nixlangParser.BinaryOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#binaryOp}.
	 * @param ctx the parse tree
	 */
	void exitBinaryOp(nixlangParser.BinaryOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void enterMethodCall(nixlangParser.MethodCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void exitMethodCall(nixlangParser.MethodCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#builtinMethod}.
	 * @param ctx the parse tree
	 */
	void enterBuiltinMethod(nixlangParser.BuiltinMethodContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#builtinMethod}.
	 * @param ctx the parse tree
	 */
	void exitBuiltinMethod(nixlangParser.BuiltinMethodContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(nixlangParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(nixlangParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#arraySliceExpression}.
	 * @param ctx the parse tree
	 */
	void enterArraySliceExpression(nixlangParser.ArraySliceExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#arraySliceExpression}.
	 * @param ctx the parse tree
	 */
	void exitArraySliceExpression(nixlangParser.ArraySliceExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#arrayExpression}.
	 * @param ctx the parse tree
	 */
	void enterArrayExpression(nixlangParser.ArrayExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#arrayExpression}.
	 * @param ctx the parse tree
	 */
	void exitArrayExpression(nixlangParser.ArrayExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpression(nixlangParser.UnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpression(nixlangParser.UnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#expressionName}.
	 * @param ctx the parse tree
	 */
	void enterExpressionName(nixlangParser.ExpressionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#expressionName}.
	 * @param ctx the parse tree
	 */
	void exitExpressionName(nixlangParser.ExpressionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#ambiguousName}.
	 * @param ctx the parse tree
	 */
	void enterAmbiguousName(nixlangParser.AmbiguousNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#ambiguousName}.
	 * @param ctx the parse tree
	 */
	void exitAmbiguousName(nixlangParser.AmbiguousNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary(nixlangParser.PrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary(nixlangParser.PrimaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(nixlangParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(nixlangParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBool(nixlangParser.BoolContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBool(nixlangParser.BoolContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(nixlangParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(nixlangParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#floatNumber}.
	 * @param ctx the parse tree
	 */
	void enterFloatNumber(nixlangParser.FloatNumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#floatNumber}.
	 * @param ctx the parse tree
	 */
	void exitFloatNumber(nixlangParser.FloatNumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link nixlangParser#integer}.
	 * @param ctx the parse tree
	 */
	void enterInteger(nixlangParser.IntegerContext ctx);
	/**
	 * Exit a parse tree produced by {@link nixlangParser#integer}.
	 * @param ctx the parse tree
	 */
	void exitInteger(nixlangParser.IntegerContext ctx);
}