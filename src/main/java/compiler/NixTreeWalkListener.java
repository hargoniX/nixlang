package compiler;

import antlr.nixlangBaseListener;
import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import compiler.utils.bytecode.Variable;
import compiler.utils.bytecode.bytecodeGenerators.*;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

public class NixTreeWalkListener extends nixlangBaseListener implements Opcodes {
    public MethodVisitor mv;
    public ClassWriter cw = new ClassWriter(0);
    private Queue<Instruction> instructionQueue = new ArrayDeque<>();
    private ArrayList<Variable> variables= new ArrayList<>();

    public NixTreeWalkListener() {
        super();
        cw.visit(52, ACC_PUBLIC + ACC_SUPER, "Test", null, "java/lang/Object", null);
        // default constructor
        MethodVisitor constructor = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
        constructor.visitCode();
        Label l0 = new Label();
        constructor.visitLabel(l0);
        constructor.visitVarInsn(ALOAD, 0);
        constructor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
        constructor.visitInsn(RETURN);
        constructor.visitMaxs(1, 1);
        constructor.visitEnd();

        mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
        mv.visitCode();
        l0 = new Label();
        mv.visitLabel(l0);

    }

    public Queue<Instruction> getInstructionQueue() {
        return instructionQueue;
    }

    @Override public void exitBlock(nixlangParser.BlockContext ctx) { }

    @Override public void exitStatement(nixlangParser.StatementContext ctx) { }

    @Override public void exitForStmnt(nixlangParser.ForStmntContext ctx) { }


    @Override public void exitIfStmnt(nixlangParser.IfStmntContext ctx) { }


    @Override public void exitWhileStmnt(nixlangParser.WhileStmntContext ctx) { }


    @Override public void exitIfThenStmnt(nixlangParser.IfThenStmntContext ctx) { }


    @Override public void exitElseStmnt(nixlangParser.ElseStmntContext ctx) { }


    @Override public void exitElifStmnt(nixlangParser.ElifStmntContext ctx) { }


    @Override public void exitAssignment(nixlangParser.AssignmentContext ctx) {
        instructionQueue.add(new AssignmentGenerator(ctx, this.variables));
    }


    @Override public void exitAssignOperator(nixlangParser.AssignOperatorContext ctx) { }


    @Override public void exitBinaryOp(nixlangParser.BinaryOpContext ctx) {

    }

    @Override public void exitExpression(nixlangParser.ExpressionContext ctx) {
        if(ctx.binaryOp() != null) {
            instructionQueue.add(new ExpressionGenerator(ctx));
        }
    }


    @Override public void exitArraySliceExpression(nixlangParser.ArraySliceExpressionContext ctx) { }


    @Override public void exitArrayExpression(nixlangParser.ArrayExpressionContext ctx) { }

    @Override public void enterMethodCall(nixlangParser.MethodCallContext ctx){
        instructionQueue.add(new InitMethodCallGenerator(ctx));
    }

    @Override public void exitMethodCall(nixlangParser.MethodCallContext ctx){
        instructionQueue.add(new MethodCallGenerator(ctx));
    }

    @Override public void exitUnaryExpression(nixlangParser.UnaryExpressionContext ctx) { }


    @Override public void exitExpressionName(nixlangParser.ExpressionNameContext ctx) { }


    @Override public void exitAmbiguousName(nixlangParser.AmbiguousNameContext ctx) { }



    @Override public void exitLiteral(nixlangParser.LiteralContext ctx) {
        instructionQueue.add(new LiteralGenerator(ctx));
    }

    @Override public void exitBool(nixlangParser.BoolContext ctx) {
        instructionQueue.add(new BooleanGenerator(ctx));
    }

    @Override public void exitString(nixlangParser.StringContext ctx) {
        instructionQueue.add(new StringGenerator(ctx));
    }

    @Override public void exitFloatNumber(nixlangParser.FloatNumberContext ctx) {
        instructionQueue.add(new DoubleGenerator(ctx));
    }

    @Override public void exitInteger(nixlangParser.IntegerContext ctx) {
        instructionQueue.add(new IntegerGenerator(ctx));
    }

    @Override public void visitTerminal(TerminalNode node) { }

    @Override public void visitErrorNode(ErrorNode node) { }
}