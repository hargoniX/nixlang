package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import compiler.utils.bytecode.exceptions.NotImplementedException;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class InitMethodCallGenerator implements Instruction, Opcodes {
    private nixlangParser.MethodCallContext ctx;

    public InitMethodCallGenerator(nixlangParser.MethodCallContext ctx){
        this.ctx = ctx;
    }

    @Override
    public void apply(MethodVisitor mv) {
        if(ctx.builtinMethod() != null){
            String method = ctx.builtinMethod().getText();
            switch (method){
                case("print"): mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"); break;
            }
        }
        else if(ctx.IDENTIFIER() != null){
            throw new NotImplementedException();
        }
        else{
            throw new NotImplementedException();
        }
    }
}