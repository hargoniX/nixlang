package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.MethodVisitor;

public class IntegerGenerator implements Instruction, Opcodes {
    private nixlangParser.IntegerContext ctx;

    public IntegerGenerator(nixlangParser.IntegerContext ctx){
        this.ctx = ctx;
    }

    @Override
    public void apply(MethodVisitor mv) {
        mv.visitLdcInsn(Integer.parseInt(ctx.getText()));
    }
}
