package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class BooleanGenerator implements Instruction, Opcodes {
    private nixlangParser.BoolContext ctx;

    public BooleanGenerator(nixlangParser.BoolContext ctx){
        this.ctx = ctx;
    }

    @Override
    public void apply(MethodVisitor mv) {
        switch (ctx.getText()){
            case("true"):   mv.visitInsn(ICONST_1); break;
            case("false"):  mv.visitInsn(ICONST_0); break;
        }
    }
}
