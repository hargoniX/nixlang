package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.exceptions.NotImplementedException;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import compiler.utils.bytecode.Type;

class TypeCast {

    static void typeCast(MethodVisitor mv, int original, int target){
        if(original == 0 && target == 1){
            mv.visitInsn(Opcodes.I2D);
        }
        else if(original == 0 && target == 2){
            mv.visitInsn(Opcodes.I2B);
        }
        else if(original == 1 && target == 0){
            mv.visitInsn(Opcodes.D2I);
        }
        else if(original == 1 && target == 2){
            mv.visitInsn(Opcodes.I2B);
        }
    }

    static int getExpressionType(nixlangParser.ExpressionContext ctx){
        if(ctx.expression().size() != 0){
            return getExpressionType(ctx.expression(0));
        }
        else if(ctx.unaryExpression() != null){
            nixlangParser.UnaryExpressionContext unaryExpression = ctx.unaryExpression();
            if(unaryExpression.unaryExpression() != null){
                return getUnaryExpressionType(unaryExpression);
            }
            else if(unaryExpression.primary() != null){
                return getPrimaryExpressionType(unaryExpression.primary());

            }
            else {
                throw new NotImplementedException();
            }
        }
        else{
            throw new NotImplementedException();
        }
    }
    private static int getUnaryExpressionType(nixlangParser.UnaryExpressionContext ctx){
        if(ctx.unaryExpression() != null){
            return getUnaryExpressionType(ctx);
        }
        else if(ctx.primary() != null){
            return getPrimaryExpressionType(ctx.primary());
        }
        else{
            throw new NotImplementedException();
        }
    }

    private  static int getLiteralExpressionType(nixlangParser.LiteralContext ctx){
        if(ctx.integer() != null){
            return Type.INTEGER.getValue();
        }
        else if(ctx.floatNumber() != null){
            return Type.DOUBLE.getValue();
        }
        else if(ctx.NONE() != null){
            return Type.NONE.getValue();
        }
        else if(ctx.string() != null){
            return Type.STRING.getValue();
        }
        else if(ctx.bool() != null){
            return Type.BOOLEAN.getValue();
        }
        else{
            return -1;
        }
    }

    private static int getPrimaryExpressionType(nixlangParser.PrimaryContext ctx){
        if(ctx.expression() != null){
            return getExpressionType(ctx.expression());
        }
        else if(ctx.literal() != null){
            return getLiteralExpressionType(ctx.literal());
        }
        else{
            throw new NotImplementedException();
        }
    }

}
