package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import compiler.utils.bytecode.Type;
import compiler.utils.bytecode.exceptions.NotImplementedException;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import static compiler.utils.bytecode.bytecodeGenerators.TypeCast.getExpressionType;

public class MethodCallGenerator implements Instruction, Opcodes {
    private nixlangParser.MethodCallContext ctx;

    public MethodCallGenerator(nixlangParser.MethodCallContext ctx){
        this.ctx = ctx;
    }

    @Override
    public void apply(MethodVisitor mv) {
        if(ctx.builtinMethod() != null){
            switch (ctx.builtinMethod().getText()){
                case("print"): handleBuiltinPrint(mv);
            }
        }
    }

    private void handleBuiltinPrint(MethodVisitor mv){
        if(ctx.expression() != null){
            for(nixlangParser.ExpressionContext expression:ctx.expression()){
                int type = getExpressionType(expression);
                if(type == Type.INTEGER.getValue()){
                    mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(I)V");
                }
                else if(type == Type.DOUBLE.getValue()){
                    mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(D)V");
                }
                else if(type == Type.STRING.getValue()){
                    mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V");
                }
                else{
                    throw new NotImplementedException();
                }
            }
        }
        else{
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "()V");
        }
    }
}
