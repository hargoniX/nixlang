package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.MethodVisitor;

public class DoubleGenerator implements Instruction, Opcodes {
    private nixlangParser.FloatNumberContext ctx;

    public DoubleGenerator(nixlangParser.FloatNumberContext ctx){
        this.ctx = ctx;
    }

    @Override
    public void apply(MethodVisitor mv) {
        mv.visitLdcInsn(Double.parseDouble(ctx.getText()));
    }
}