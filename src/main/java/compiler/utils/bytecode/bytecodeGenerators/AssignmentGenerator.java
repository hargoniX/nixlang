package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import compiler.utils.bytecode.Type;
import compiler.utils.bytecode.Variable;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.ArrayList;

import static compiler.utils.bytecode.bytecodeGenerators.TypeCast.getExpressionType;


public class AssignmentGenerator implements Instruction, Opcodes {
    private nixlangParser.AssignmentContext ctx;
    private ArrayList<Variable> variables;

    public AssignmentGenerator(nixlangParser.AssignmentContext ctx,ArrayList<Variable> variables){
        this.ctx = ctx;
        this.variables = variables;
    }

    @Override
    public void apply(MethodVisitor mv) {
        int pos = -1;
        int type = getExpressionType(ctx.expression());
        for(Variable var: variables){
            if(var.getName().equals(ctx.IDENTIFIER().getText())){
                pos = variables.indexOf(var);
                int varNumber = variables.indexOf(var);
            }
        }
        if(pos == -1){
            pos = variables.size();
            variables.add(new Variable(type, ctx.IDENTIFIER().getText()));
        }
        else{
            variables.get(pos).setType(type);
        }

        if(type == Type.INTEGER.getValue() || type == Type.BOOLEAN.getValue()){
            mv.visitIntInsn(ISTORE, pos);
        }
        else if(type == Type.DOUBLE.getValue()){
            mv.visitIntInsn(DSTORE, pos);
        }
        else{
            mv.visitIntInsn(ASTORE, pos);
        }
    }
}
