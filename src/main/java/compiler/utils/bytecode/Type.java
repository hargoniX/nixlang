package compiler.utils.bytecode;

public enum Type {
    INTEGER(0), DOUBLE(1), BOOLEAN(2), STRING(3), NONE(4);

    private int value;
    Type(int type){
        this.value = type;
    }

    public int getValue() {
        return value;
    }
}
